﻿using UnityEngine;
using System.Collections;

public class moveBarre : MonoBehaviour
{

		public enum blockDirection
		{
				free,
				front,
				back
		}

		public blockDirection blockdir = blockDirection.free;

		public float mouseX;
		public float mouseY;
	
		public int barreIndex;
		public int mouseIndex;

		public BabyBarre barre;
		public equipe team;
		private float z_min;
		private float z_max;
		private float posZ;
		private int invert = 1;
	
		private float sensitivity = 200f;

		public bool bloque = false;

		public float speedX = 5;
		public float speedY = 5;	
	
	
		// Use this for initialization
		IEnumerator Start ()
		{
				
				yield return new WaitForEndOfFrame ();

		
				if (transform.parent.name == "Equipe2") {
						invert = -1;
				}
		

				switch (gameObject.tag) {
				case "attaquants":
						z_min = -2.9f;
						z_max = 2.9f;
						break;
				case "gardien":
						z_min = -2.2f;
						z_max = 2.2f;
						break;
				case "centres":
						z_min = -2f;
						z_max = 2f;
						break;
				case "defenseurs":
						z_min = -4f;
						z_max = 4f;
						break;
				}
		
		
		}
	
		// Update is called once per frame
		void Update ()
		{

				if (barreInputs.souris.ContainsKey ("mouse" + mouseIndex + "X") && barreInputs.souris.ContainsKey ("mouse" + mouseIndex + "Y")) {
						mouseX = (float)barreInputs.souris ["mouse" + mouseIndex + "X"] * speedX * Time.deltaTime * sensitivity * invert;
						mouseY = (float)barreInputs.souris ["mouse" + mouseIndex + "Y"] * speedY * Time.deltaTime * sensitivity * invert; 
				}
			

				if (blockdir == blockDirection.front && mouseX > 0) {
						mouseX = 0;
				}
		
				if (blockdir == blockDirection.back && mouseX < 0) {
						mouseX = 0;
				}

				transform.Rotate (0, 0, mouseX, Space.Self);

				transform.Translate (0, 0, mouseY, Space.World);	
			
			
				posZ = Mathf.Clamp (transform.position.z, z_min, z_max);
				transform.position = new Vector3 (transform.position.x, transform.position.y, posZ);
		
		}

		public void blockBarre ()
		{
				bloque = true;
				if (transform.localRotation.eulerAngles.z < 180) {
						blockdir = blockDirection.back;
				} else {
						blockdir = blockDirection.front;
				}
		}

		public void detach ()
		{
				bloque = false;
				blockdir = blockDirection.free;
		}

}
