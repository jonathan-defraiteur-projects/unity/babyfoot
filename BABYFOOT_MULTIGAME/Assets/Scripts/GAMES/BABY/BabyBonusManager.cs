﻿using UnityEngine;
using System.Collections;

public class BabyBonusManager : MonoBehaviour {
	
	public static BabyBonusManager instance;

	public GameObject[] bonus ;
	Vector3 posBallon ; 
	Vector3 velBallon ;
	Vector3 angVelBallon ;
	
	public bool actif = false ;
	public float activeTime = 20 ;

	
	float timer = 0 ;

	
	// BONUS
	bool bonusOn = true;
	public int bonusTeam = -1;

	public GameObject bonusGeneratorPrefab;
	public float timer_max;	
	public float randRange = 5;
	float actualRand = 0;

	public float nextRemoveTime;
	GameObject generatorInstance;




	void Awake(){

		instance = this;

	}


	
	void Update () {
		bonusTimer();

		if (actif && Time.time > nextRemoveTime) {
			removeBonusGenerator();
		}
	}

	
	
	public void genererBonus(int id ){
		CancelInvoke();
		//GameObject bonusGenere = (GameObject)Instantiate(selectedBonus,BabyManager.instance.spawnPosition.position,Quaternion.identity);
		
		//bonusGenere.SendMessage("teamNumber" , id);
		
		actif = false;
		//BabyManager.instance.gameMode = selectedBonus.name;

		Destroy(gameObject);
	}

	
	void bonusTimer(){

		if(actualRand == 0){
			actualRand = UnityEngine.Random.Range(-randRange,randRange);
		}

		if(!actif && BabyManager.instance.gameMode == "normal" && bonusOn && BabyManager.instance.inGame){
			timer += Time.deltaTime;
			
			if(timer > (timer_max + actualRand)){
				initBonusGenerator();
			}
			
		}else{
			reset();
		}
	}
	
	
	



	void initBonusGenerator(){
		actif = true;
		generatorInstance = BabyManager.instance.replaceBall (BabyManager.instance.ballons [0], bonusGeneratorPrefab);
		nextRemoveTime = Time.time + activeTime;
	}
	
	
	void removeBonusGenerator(){

		actif = false;
		reset();
		if(BabyManager.instance.ballons.Contains(generatorInstance)){
			BabyManager.instance.replaceBall(BabyManager.instance.ballons[BabyManager.instance.ballons.IndexOf(generatorInstance)] , BabyManager.instance.ballonPrefab);
		}

	}

	
	public void reset(){
		timer = 0;
		actualRand = 0;
	}
	


}




