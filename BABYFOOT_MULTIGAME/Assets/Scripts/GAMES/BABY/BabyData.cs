﻿using UnityEngine;
using System.Collections;

// new libraries
using System;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Linq;




public class BabyData : MonoBehaviour {
	
	
	public static BabyData instance;
	
	public BabySaveData gameInfo = new BabySaveData();

	
	void Awake () {
		instance = this;
	}
	
	void Start(){
		Load();
		BabyManager.instance.GameInfos = gameInfo;
		
	}
	
	public void Save()
	{
		BinaryFormatter bf = new BinaryFormatter();
		// On ouvre un fichier playerInfo.dat qu'on crée s'il n'existe pas
		FileStream file = File.Create(Application.streamingAssetsPath + "/save/gameData.dat");//Application.persistentDataPath
		
		BabySaveData data = gameInfo;
		
		bf.Serialize (file, data);
		file.Close();
		Debug.Log("Saved at : " + Application.streamingAssetsPath + "/save/gameData.dat"); //Application.persistentDataPath
	}
	
	
	
	public void Load()
	{
		// On vérifie que le fichier de sauvegarde existe
		if (File.Exists ( Application.streamingAssetsPath + "/save/gameData.dat")) //Application.persistentDataPath
		{
			
			BinaryFormatter bf = new BinaryFormatter();
			FileStream file = File.Open( Application.streamingAssetsPath + "/save/gameData.dat" , FileMode.Open);//Application.persistentDataPath
			
			gameInfo = (BabySaveData)bf.Deserialize(file);
			file.Close();
			
			
			
		}else{
			gameInfo = new BabySaveData();

		}
		
	}
	

}

[Serializable]
public class BabySaveData {
	public float scoreMax = 5;
	public float scoreEcart = 2;
}

[Serializable]
public enum equipe{
	equipe1,
	equipe2
}

[Serializable]
public class BabyTeam{
	public equipe equipe;
	public int score = 0;
	public GameObject barresContainer;
	public GameObject playersContainer;

	public List<BabyBarre> barres = new List<BabyBarre>();
}

[Serializable]
public class BabyBarre{
	public GameObject barreOBject;
	public bool blocked;
	public List<BabyPlayer> players = new List<BabyPlayer>();
}

[Serializable]
public class BabyPlayer{
	public GameObject playerOBject;
}
