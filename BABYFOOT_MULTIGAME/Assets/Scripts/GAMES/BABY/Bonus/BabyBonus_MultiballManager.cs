﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class BabyBonus_MultiballManager : MonoBehaviour
{
	
		public int numberOfBalls = 5;
		public GameObject ballPrefab;
		public float timeBetweenSpawns = 1;
		public List<BoxCollider> zones = new List<BoxCollider> ();
		public Vector3 startPos;
		public List<BoxCollider> tempZones = new List<BoxCollider> ();
		


		void Start ()
		{	
				transform.position = startPos;
				StartCoroutine (Spawn ());
			

		}
		// Use this for initialization
		IEnumerator Spawn ()
		{
				tempZones = zones;
				for (int i = 0; i < numberOfBalls; i++) {
						yield return new WaitForSeconds (timeBetweenSpawns);
						BoxCollider zoneToAppear = tempZones [Random.Range (0, tempZones.Count - 1)];
						Vector3 bounds = zoneToAppear.bounds.extents;
						Vector3 posToAppear = zoneToAppear.transform.position + 
								new Vector3 (0, -bounds.y, Random.Range ((float)-bounds.z, (float)bounds.z));
						
						Instantiate (ballPrefab, posToAppear, Quaternion.identity);
						tempZones.Remove (zoneToAppear);
				}
		}
	
		// Update is called once per frame
		void Update ()
		{
			
		}

}
