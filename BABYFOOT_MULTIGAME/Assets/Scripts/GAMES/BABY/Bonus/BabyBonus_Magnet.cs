﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class BabyBonus_Magnet : MonoBehaviour
{

		Transform closestPlayer;
		Transform target;
		Vector3 dir;
		bool balle_au_pied;
		float dist_balle_pied;
		Color ambient_defaut = new Color () ;
		float default_intensity;

		public Color black ;
		public Color ambient ;
		public bool aimant ;
	
		public int team;
	
		public GameObject bonusLight;

		public GameObject babyfoot;
	
	
		bool actif;

		public float minDist = 2;
		public int magnetForce = 3000;
		public int expulseVelocity = 150;
		List<GameObject> bonusLights = new List<GameObject> ();

		// Use this for initialization
		void Start ()
		{
				team = BabyBonusManager.instance.bonusTeam;
				actif = true;
				babyfoot = GameObject.Find ("BABYFOOT");
				init ();
		}


	
		// Update is called once per frame
		void Update ()
		{
				if (actif) {
						target = findClosest ();
						if (target) {

								dir = (target.position - transform.position).normalized;

								if (dist_balle_pied < minDist) {
										rigidbody.AddForce (dir * magnetForce);
								}

								dist_balle_pied = Vector3.Distance (target.position, transform.position);
				
				
								balle_au_pied = dist_balle_pied < 1;
				
				
								if (Input.GetButtonDown ("violet_" + team) && balle_au_pied) {
										rigidbody.velocity = -dir * expulseVelocity;
										audio.Play ();
								}

						}
				}
		}


	
	
		void init ()
		{

				ambient_defaut = RenderSettings.ambientLight;
				default_intensity = GameObject.FindGameObjectWithTag ("classicLight").light.intensity;


				if (team != -1) {
						GameObject[] joueurs = GameObject.FindGameObjectsWithTag ("team" + team);
			
						foreach (GameObject joueur in joueurs) {
								GameObject lampe = (GameObject)Instantiate (bonusLight, joueur.transform.position, joueur.transform.rotation);
								lampe.transform.parent = joueur.transform;
								bonusLights.Add (lampe);
						}
			
				}

				// Changer éclairage	
				changeAmbient (ambient_defaut, ambient, 1.5f);
				changeLightIntensity (default_intensity, 0.1f, 1.5f);
		
		
				// Changer pitch foule
				changeCrowdPitch (0.3f, 1.5f);

		}
	

		public void fin ()
		{
				gameObject.tag = "inactive";
				actif = false;
				BabyBonusManager.instance.actif = false;
				Destroy (rigidbody);
				collider.enabled = false;
				BabyManager.instance.gameMode = "normal";
		
				foreach (Transform child in transform) {
						Destroy (child.gameObject);
				}
		

				foreach (GameObject lampe in bonusLights) {
						Destroy (lampe);
				}
				bonusLights.Clear ();
		
				// Changer éclairage	
				changeAmbient (ambient, ambient_defaut, 1.5f);
				changeLightIntensity (0.1f, default_intensity, 1.5f);
		
				// Changer pitch foule
				changeCrowdPitch (1, 1.5f);

		}



		public void changeAmbient (Color from, Color to, float time)
		{

				Hashtable args = new Hashtable ();

				args.Add ("from", from);
				args.Add ("to", to);
				args.Add ("time", time);
				args.Add ("onupdate", "updateAmbient");

				iTween.ValueTo (gameObject, args);
		

		}
	
	
		public void changeLightIntensity (float from, float to, float time)
		{

				Hashtable args = new Hashtable ();

				args.Add ("from", from);
				args.Add ("to", to);
				args.Add ("time", time);
				args.Add ("onupdate", "updateIntensity");


				iTween.ValueTo (gameObject, args);
		
				
		
		}
	
	
	
		void changeCrowdPitch (float to, float time)
		{
		
				Hashtable args = new Hashtable ();
				args.Add ("pitch", to);
				args.Add ("time", time);
				args.Add ("easetype", "easeInQuint");

				iTween.AudioTo (babyfoot, args);
			
		}
	
	
	
		Transform findClosest ()
		{

				Transform new_target = GameObject.FindGameObjectsWithTag ("team" + team)
				.Where (obj => Vector3.Distance (obj.transform.position, transform.position) < minDist)
				.OrderBy (obj => Vector3.Distance (obj.transform.position, transform.position))
				.Select (obj => obj.transform)
				.FirstOrDefault ();


				return new_target;

		}

	
		void updateAmbient (Color x)
		{
				RenderSettings.ambientLight = x;
		}
	

		void updateIntensity (float x)
		{
				GameObject.FindGameObjectWithTag ("classicLight").light.intensity = x;
		}
	

}










