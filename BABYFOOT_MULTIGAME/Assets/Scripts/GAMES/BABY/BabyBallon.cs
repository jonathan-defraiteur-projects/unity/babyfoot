﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;


public class BabyBallon : MonoBehaviour
{

		public List<Collision> collisions = new List<Collision> ();
		public float touchAngle = 0;
		public Transform meshObj;
		public List<GameObject> colsObj = new List<GameObject> ();

		public bool attachedToBarre = false;
		moveBarre attachedBarre;

		Vector3 valueToRotate;
		float asleepTime;

		bool isWakingUp = false;
		void Start ()
		{
				BabyManager.instance.ballons.Add (gameObject);

		}

	
		void Update ()
		{
			
				if (attachedToBarre && meshObj) {
						

						
						valueToRotate = rigidbody.velocity * 360 * Time.deltaTime * 1 / (2 * Mathf.PI * GetComponent<SphereCollider> ().radius);
						
						meshObj.Rotate (valueToRotate);
				
				}
				if (!isWakingUp && rigidbody.IsSleeping ()) {
						isWakingUp = true;
						StartCoroutine ("WakeMeUp");
				}
		}

		

		
		
		void OnCollisionEnter (Collision col)
		{
		
		
				if (!collisions.Contains (col)) {
			
						collisions.Add (col);
						colsObj.Add (col.gameObject);
				}

				if (col.gameObject.layer == LayerMask.NameToLayer ("joueur") || col.gameObject.layer == LayerMask.NameToLayer ("pied")) {
						audio.volume = col.relativeVelocity.magnitude / 10;
						audio.Play ();
				}

				if (col.gameObject.layer == LayerMask.NameToLayer ("pied") && !attachedToBarre) {
						checkForBlock (col);
				}

		}




		void OnCollisionExit (Collision collision)
		{
				if (collisions.Contains (collision))
						collisions.Remove (collision);
				colsObj.Remove (collision.gameObject);
				if (collision.gameObject.layer == LayerMask.NameToLayer ("terrain")) {
						DetachMe ();
				}
		}
		

		void OnJointBreak ()
		{
				DetachMe ();
		}


		/*----------------------
	BLOCAGE DE BALLE
	----------------------*/

		void checkForBlock (Collision col)
		{
				Collision terrainCol = collisions.Where (obj => obj.gameObject.layer == LayerMask.NameToLayer ("terrain")).Select (obj => obj).FirstOrDefault ();
				if (terrainCol != null) {

						if (Vector3.Dot (col.contacts [0].normal, Vector3.down) > 0.75f) {



								Transform player = col.transform.parent;
								


								var barre = from equipe in BabyManager.instance.equipes

								from b in equipe.barres
									from p in b.players
									where p.playerOBject == player.gameObject
									select b.barreOBject;

								attachedBarre = barre.FirstOrDefault ().GetComponent<moveBarre> ();
								attachedBarre.blockBarre ();
								attachedToBarre = true;

								FixedJoint j = gameObject.AddComponent<FixedJoint> ();
								j.connectedBody = attachedBarre.rigidbody;
								
								j.breakForce = 100;
								//	StartCoroutine ("Detach");

						}

				}

		}

	
		IEnumerator Detach (int timeToWait)
		{
				yield return new WaitForSeconds (timeToWait);
				if (attachedBarre) {
						if (gameObject.GetComponent<FixedJoint> ())
								Destroy (gameObject.GetComponent<FixedJoint> ());
						attachedBarre.detach ();
						attachedBarre = null;
						attachedToBarre = false;
				}
		}

		IEnumerator WakeMeUp ()
		{
				yield return new WaitForSeconds (3);
				if (rigidbody.IsSleeping ())
						rigidbody.AddTorque (Vector3.right * 360);
				isWakingUp = false;
		}
	
		void DetachMe ()
		{
				StopCoroutine ("Detach");
				if (attachedBarre) {
						if (gameObject.GetComponent<FixedJoint> ())
								Destroy (gameObject.GetComponent<FixedJoint> ());
						attachedBarre.detach ();
						attachedBarre = null;
						attachedToBarre = false;
				}
		}
	
		void OnDestroy ()
		{
				if (BabyManager.instance.ballons.Contains (gameObject))
						BabyManager.instance.ballons.Remove (gameObject);
		}

		
}
