﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
public class BabyManager : MonoBehaviour
{

		public static BabyManager instance;

		public GameObject ballonPrefab;
		public List<GameObject> ballons = new List<GameObject> ();


		public AudioClip[] jingles;
		public AudioSource jingleplayer;

		public List<BabyTeam> equipes = new List<BabyTeam> (2);

		public BabySaveData GameInfos;

			
		public string gameMode = "normal";
		public bool inGame = false; // true pendant le jeu
		public bool endGame = false; // false pendant le jeu


		public Transform spawnPosition;

		void Awake ()
		{
				instance = this;
		}

		void Start ()
		{
				BabyData.instance.Load ();
				GameInfos = BabyData.instance.gameInfo;

				initTeams ();

				int direction = UnityEngine.Random.Range ((int)0, (int)2); // Pour le premier lancé de balle la direction est Random
		
				StartCoroutine (respawnBall (ballonPrefab, direction));	
		}


		public void initTeams ()
		{
				for (int i = 0; i < equipes.Count; i++) {
						equipes [i].score = 0;
						Transform barresContainer = equipes [i].barresContainer.transform;
						Transform playersContainer = equipes [i].playersContainer.transform;
						equipes [i].barres.Clear ();
						foreach (Transform barre in barresContainer) {
								BabyBarre b = new BabyBarre ();
								b.barreOBject = barre.gameObject;

								Transform players = playersContainer.FindChild (barre.name);
								foreach (Transform player in players) {
										BabyPlayer p = new BabyPlayer ();
										p.playerOBject = player.gameObject;
										b.players.Add (p);
								}

								equipes [i].barres.Add (b);
						}


				}
		}


		// Quand on marque un but
		public void but (int equipe, GameObject balleAuBut)
		{
				if (ballons.Contains (balleAuBut))
						ballons.Remove (balleAuBut);
		
				BabyBonusManager.instance.reset ();
		
				BabyBonusManager.instance.bonusTeam = -1;


				if (inGame) {
						if (ballons.Count < 1) {
								inGame = false;
						}
						StartCoroutine ("exciteCrowd");
						equipes [equipe].score++;
						GameObject ballToRespawn;
						// On vérifie qu'on a pas atteint le nombre de buts Max
						comparePoints ();

						if (balleAuBut.tag == "bonusGenerator") {
								BabyBonusManager.instance.bonusTeam = equipe;
								ballToRespawn = BabyBonusManager.instance.bonus [UnityEngine.Random.Range (0, BabyBonusManager.instance.bonus.Length - 1)];
								gameMode = ballToRespawn.name;
						} else {
								StartCoroutine ("calmCrowd");
								ballToRespawn = ballonPrefab;
						}
			
			
						if (balleAuBut.tag == "bonus") {
								//gameMode="normal";
								balleAuBut.SendMessage ("fin");
				
						}
			
						if (!endGame) {
								playJingle ();
								if (ballons.Count < 1) {
										StartCoroutine (respawnBall (ballToRespawn, equipe));
								}
						}

				}



				Destroy (balleAuBut, 2);

		}





		// Renvois la balle depuis le centre
		public IEnumerator respawnBall (GameObject ballToRespawn, int _id)
		{
				yield return new WaitForSeconds (2f);
				if (!endGame) {
						inGame = true;
				
						GameObject ballon = (GameObject)Instantiate (ballToRespawn, spawnPosition.position, spawnPosition.rotation);

						float multiplier = (_id - 0.5f) * 2;
						if (ballon.rigidbody)
								ballon.rigidbody.AddTorque (0, 0, 20000 * multiplier, ForceMode.VelocityChange);

				}
		}



	
		public GameObject replaceBall (GameObject ballToReplace, GameObject replacementBall)
		{

				Vector3 posBallon = ballToReplace.transform.position;
				Vector3 velBallon = ballToReplace.rigidbody.velocity;
				Vector3 angVelBallon = ballToReplace.rigidbody.angularVelocity;
				Destroy (ballToReplace);
			
				GameObject newBall = (GameObject)Instantiate (replacementBall, posBallon, Quaternion.identity);
				newBall.rigidbody.velocity = velBallon;
				newBall.rigidbody.angularVelocity = angVelBallon;
			
				return newBall;
		}

		// Compare les points et indique si il y a un gagnant
		void comparePoints ()
		{

				if (
			Mathf.Max (equipes [0].score, equipes [1].score) >= GameInfos.scoreMax 
						&& (Mathf.Max (equipes [0].score, equipes [1].score) - Mathf.Min (equipes [0].score, equipes [1].score)) >= GameInfos.scoreEcart) { //Si le score 0 dépasse
						win ();
				}

		}

		void win ()
		{
				endGame = true;
				inGame = false;
		}



	
	
	
		void playJingle ()
		{
				if (jingles.Length > 0) {
						jingleplayer.clip = jingles [UnityEngine.Random.Range (0, jingles.Length)];
						jingleplayer.Play ();
				}
		}
	

	
		IEnumerator exciteCrowd ()
		{
		
				Hashtable args = new Hashtable ();
		
				args.Add ("volume", 1);
				args.Add ("time", 2);
				args.Add ("easetype", "easeInOutSine");
		
		
				iTween.AudioTo (GameObject.Find ("BABYFOOT"), args);	
		
				return null;
		}
	
		IEnumerator calmCrowd ()
		{
				Hashtable args = new Hashtable ();
		
				args.Add ("delay", 4);
				args.Add ("volume", 0.2);
				args.Add ("time", 6);
				args.Add ("easetype", "easeInOutSine");
		
				iTween.AudioTo (GameObject.Find ("BABYFOOT"), args);	
		
				return null;
		}




}