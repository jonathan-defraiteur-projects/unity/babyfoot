﻿using UnityEngine;
using System.Collections;

public class butPong : MonoBehaviour {

	public managerPong.select_equipe equipe;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void OnTriggerEnter (Collider col) {
		if(col.gameObject.layer == LayerMask.NameToLayer("ballon")){

			if(equipe == managerPong.select_equipe.equipe1){
				managerPong.instance.addBut(managerPong.instance.equipe1 , col.transform.parent.GetComponent<palet>());
			}else if(equipe == managerPong.select_equipe.equipe2){
				managerPong.instance.addBut(managerPong.instance.equipe2 , col.transform.parent.GetComponent<palet>());
			}

			audio.Play();
		}
	}
}
