﻿using UnityEngine;
using System.Collections;

public class moveBarrePong : MonoBehaviour {

	
	public float mouseX;
	public float mouseY;
	
	public int barreIndex;
	public int mouseIndex;
	

	
	public float z_min;
	public float z_max;
	private float posZ;
	private int invert = 1;
	
	private float sensitivity =200f;

	

	public float speedX = 5;
	public float speedY = 5;	
	
	
	// Use this for initialization
	IEnumerator Start () {
		
		yield return new WaitForEndOfFrame();

		
		if(transform.parent.name == "Equipe2"){
			invert=-1;
		}
		
		
		

		
		
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		

		
		if (barreInputs.souris.ContainsKey("mouse"+ mouseIndex + "X") && barreInputs.souris.ContainsKey("mouse"+ mouseIndex + "Y") && mouseIndex > 0) {
			mouseX = (float)barreInputs.souris["mouse"+ mouseIndex + "X"];
			mouseY = (float)barreInputs.souris["mouse"+ mouseIndex + "Y"];

			mouseX *= speedX * Time.fixedDeltaTime * sensitivity * invert;
			mouseY *= speedY * Time.fixedDeltaTime * sensitivity * invert;


		}
	
		transform.Rotate(0, mouseX , 0,Space.World);
				
		transform.Translate(0,0,mouseY ,Space.World);	
		
		
		posZ = Mathf.Clamp( transform.position.z , z_min , z_max );
		transform.position = new Vector3(transform.position.x , transform.position.y , posZ);
		
	}
}
