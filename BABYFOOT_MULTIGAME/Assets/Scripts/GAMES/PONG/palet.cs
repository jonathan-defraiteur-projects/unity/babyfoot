﻿using UnityEngine;
using System.Collections;

public class palet : MonoBehaviour {

	public static int nbPalets;

	void Start(){
		nbPalets++;
		managerPong.instance.listePalets.Add(this);
	}

	void OnCollisionEnter(Collision col){
		if(col.gameObject.layer == LayerMask.NameToLayer("joueur")){
			GetComponentInChildren<ParticleSystem>().Play();
		}
	}	

	void OnDestroy(){
		nbPalets--;
		managerPong.instance.listePalets.Remove(this);
	}

}
