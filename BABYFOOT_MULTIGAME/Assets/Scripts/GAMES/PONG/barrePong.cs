﻿using UnityEngine;
using System.Collections;

public class barrePong : MonoBehaviour {

	public managerPong.select_equipe equipe;

	// Use this for initialization
	void Start () {
		audio.volume = 0.3f;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnCollisionEnter(Collision col){
		if(col.gameObject.layer == LayerMask.NameToLayer("ballon")){
			audio.clip = managerPong.instance.barreImpactSounds[Random.Range((int)0,(int)managerPong.instance.barreImpactSounds.Length)];
			//audio.pitch = Random.Range(0.8f , 1.6f);
			audio.Play();
		}
	}



}
