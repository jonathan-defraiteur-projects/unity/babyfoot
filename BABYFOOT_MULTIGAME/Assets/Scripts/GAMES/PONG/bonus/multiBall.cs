﻿using UnityEngine;
using System.Collections;

public class multiBall : MonoBehaviour {

	Vector3 pos;
	// Use this for initialization
	void Start () {
		spawnPalet();
	}
	

	void Update(){
		if(palet.nbPalets <= 1){
			managerPong.instance.deactivateBonus();
			Destroy(gameObject);
		}
	}


	public void spawnPalet(){
		if(managerPong.instance.barreBonus.equipe == managerPong.select_equipe.equipe1){
			pos = managerPong.instance.setRespawn(managerPong.instance.equipe1.defenseur);
			Instantiate(managerPong.instance.paletPrefab , pos , managerPong.instance.paletPrefab.transform.rotation);
			pos = managerPong.instance.setRespawn(managerPong.instance.equipe1.centre);
			Instantiate(managerPong.instance.paletPrefab , pos , managerPong.instance.paletPrefab.transform.rotation);
			pos = managerPong.instance.setRespawn(managerPong.instance.equipe1.attaquant);
			Instantiate(managerPong.instance.paletPrefab , pos , managerPong.instance.paletPrefab.transform.rotation);
		}else{
			pos = managerPong.instance.setRespawn(managerPong.instance.equipe2.defenseur);
			Instantiate(managerPong.instance.paletPrefab , pos , managerPong.instance.paletPrefab.transform.rotation);
			pos = managerPong.instance.setRespawn(managerPong.instance.equipe2.centre);
			Instantiate(managerPong.instance.paletPrefab , pos , managerPong.instance.paletPrefab.transform.rotation);
			pos = managerPong.instance.setRespawn(managerPong.instance.equipe2.attaquant);
			Instantiate(managerPong.instance.paletPrefab , pos , managerPong.instance.paletPrefab.transform.rotation);
		}

	}




}
