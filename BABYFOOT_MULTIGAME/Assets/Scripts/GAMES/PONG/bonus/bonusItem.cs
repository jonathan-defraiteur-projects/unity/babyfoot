﻿using UnityEngine;
using System.Collections;

public class bonusItem : MonoBehaviour {

	managerPong.bonus bonus = new managerPong.bonus();

	public float speed = 2;
	public float lifeTime = 4;
	GameObject item;

	Vector3 dir;

	float creationTime;

	void Start(){
		creationTime = Time.time;
		dir = new Vector3(Random.Range(-20,20) , 0 , Random.Range(-20,20)).normalized;
		bonus = managerPong.instance.lesBonus[Random.Range((int)0,(int)managerPong.instance.lesBonus.Length)];
		item = (GameObject)Instantiate(bonus.item , transform.position , bonus.item.transform.rotation);
		item.transform.parent = transform;
		rigidbody.velocity = dir * speed;
	}


	void Update(){
		if(Time.time > creationTime + lifeTime){
			Destroy(gameObject);
		}
	}

	void OnTriggerEnter(Collider col){
		if(col.gameObject.layer == LayerMask.NameToLayer("joueur")){
			managerPong.instance.activateBonus(bonus , col.transform.parent.GetComponent<barrePong>());
			Destroy(gameObject);
		}
	}

	void OnDestroy(){
		managerPong.instance.setNextBonusTime();
		managerPong.instance.bonusItemInGame = false;
	}

}
