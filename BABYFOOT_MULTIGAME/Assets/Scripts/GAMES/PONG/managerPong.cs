﻿
using UnityEngine;
using System.Collections;
using System.Collections.Generic;



public class managerPong : MonoBehaviour {

	[System.Serializable]
	public class bonus{
		public string nom;
		public GameObject bonusObject;
		public GameObject item;
	}

	public GameObject endGameUI;

	[HideInInspector]
	public List<palet> listePalets = new List<palet>();

	public static managerPong instance;

	#region variables
	public int maxScore;
	public enum select_equipe{equipe1, equipe2}


	public AudioClip[] barreImpactSounds;
	public AudioClip[] wallImpactSounds;
	public AudioSource endGameSound;
	#region Bonus
	public GameObject bonusPrefab;
	public bonus[] lesBonus;
	[HideInInspector]
	public barrePong barreBonus;
	public bool bonusItemInGame = false;
	public bool activeBonus = false;
	public float bonusFrequency;
	public float bonusFrequencyVariation;
	float nextBonusTime;
	#endregion Bonus


	#region equipes
	[System.Serializable]
	public class equipe
		
	{
		public int score;
		public barrePong[] toutesBarres = new barrePong[4];
		public barrePong gardien;
		public barrePong defenseur;
		public barrePong centre;
		public barrePong attaquant;
		public UILabel guiScore;
		public UILabel guiScore2;
	};
	
	public equipe equipe1;
	public equipe equipe2;

	#endregion equipes


	public Transform respawnBall;

	public GameObject paletPrefab;


	public bool finDePartie = false;


	#endregion variables



	void Awake () {
		instance = this;
	}

	// Use this for initialization
	void Start () {
		resetScore();
		setNextBonusTime();
	}
	
	// Update is called once per frame
	void Update () {
		updateBonus();
	}

	void OnLevelWasLoaded() {
		resetScore();
		setNextBonusTime();
	}
	

	void resetScore(){
		equipe1.score = 0;
		equipe1.guiScore.text = equipe1.score.ToString();
		equipe1.guiScore2.text = equipe1.score.ToString();
		equipe2.score = 0;
		equipe2.guiScore.text = equipe2.score.ToString();
		equipe2.guiScore2.text = equipe2.score.ToString();
	}


	public void addBut(equipe equipe , palet pal){
		equipe.score++;
		equipe.guiScore.text = equipe.score.ToString();
		equipe.guiScore2.text = equipe.score.ToString();
		Destroy(pal.gameObject);
		setNextBonusTime();
		if(equipe.score < maxScore){

			if(palet.nbPalets > 1){

			}else{
				StartCoroutine(resetPalet(oppositeTeam(equipe)));
			}
		}else{
			endGame(equipe);
		}

	}



	public IEnumerator resetPalet(equipe equipe){

		yield return new WaitForSeconds(2);

		Vector3 respawnPos;
		if(equipe != null){
			respawnPos = setRespawn(equipe);
		}else{
			respawnPos = setRespawn();
		}


		Instantiate(paletPrefab , respawnPos , paletPrefab.transform.rotation);


	}




	public equipe oppositeTeam(equipe equipe){
		if(equipe == equipe1){
			return equipe2;
		}else if(equipe == equipe2){
			return equipe1;

		}else{
			return null;
		}
	}



	public Vector3 setRespawn(){
		return respawnBall.position;
	}

	public Vector3 setRespawn(equipe equipe){

		Vector3 pos = equipe.defenseur.transform.position;
		
		Vector3 spawnPoint = new Vector3(pos.x , 0 , -Mathf.Sign(pos.z) * 2);

		return spawnPoint;
	}

	public Vector3 setRespawn(barrePong barre){
		
		Vector3 pos = barre.transform.position;
		
		Vector3 spawnPoint = new Vector3(pos.x , 0 , -Mathf.Sign(pos.z) * 2);
		
		return spawnPoint;
	}



	public void updateBonus(){
		if(!bonusItemInGame && !activeBonus && nextBonusTime != 0){
			if(Time.time > nextBonusTime){
				spawnBonus();
				setNextBonusTime();
			}
		}
	}


	public void spawnBonus(){
		bonusItemInGame = true;
		Instantiate(bonusPrefab , Vector3.zero , bonusPrefab.transform.rotation);
	}

	public void activateBonus(bonus leBonus , barrePong barre){
		activeBonus = true;
		barreBonus = barre;

		Instantiate(leBonus.bonusObject);

	}

	
	public void deactivateBonus(){
		setNextBonusTime();
		activeBonus = false;
	}


	public void setNextBonusTime(){
		nextBonusTime = Time.time + bonusFrequency + Random.Range(-bonusFrequencyVariation,bonusFrequencyVariation);

	}

	
	public void endGame(equipe equipe){
		foreach(palet pal in listePalets){
			Destroy(pal.gameObject);
		}
		finDePartie = true;
		endGameUI.SetActive(true);
		endGameSound.Play();
	}

	public void uiRestart(){
		Application.LoadLevel(Application.loadedLevel);
	}

	public void uiMainMenu(){
		Application.LoadLevel("mainMenu");
	}

}









