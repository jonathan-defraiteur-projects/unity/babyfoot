

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class barreInputs : MonoBehaviour  {

	
	public static SortedList<string,float> souris;

	public bool singleMouse;

	void Start(){
		Screen.showCursor = false;
		for(int i=0; i<9 ; i++){
			
			souris = new SortedList<string,float>();
			
			souris.Add("mouse"+i+"X",0);
			souris.Add("mouse"+i+"Y",0);

		}
	}

	void Update(){
		if(singleMouse){
			for(int i=0; i<9 ; i++){
				
				souris["mouse"+i+"X"] = Input.GetAxis("Mouse X") / 10;
				souris["mouse"+i+"Y"] = Input.GetAxis("Mouse Y") / 10;
			}
		}
	}
	
	public void OSCMessageReceived(OSC.NET.OSCMessage message){
		string address = message.Address;
		ArrayList args = message.Values;
		

		foreach( var item in args){
			string key = address.Substring(1);
			souris[key] = (float)item;
			
		}
		
	}

}

